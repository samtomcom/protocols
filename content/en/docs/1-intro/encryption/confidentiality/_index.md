---
title: "Confidentiality"
date: 2020-02-17
weight: 10
---
{{< pg_start >}}


### Confidentiality

Consider the following protocol, where we make explicit the initial knowledge of the different agents:<br/>
<ol>
   <b>Knowledge:</b> A:[secret, K], B:[K], E:[]
   <li> A -> B : {secret}K
</ol>

Let us now consider a simple interception attack. 


<div class='game'>
    <div class='agents' id='agents_box'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list'></ul>
    </div>
</div>

<div class='steps'>
    <button onclick="step10();">Step 0</button>
    <button onclick="step11();">Step 1 (A sends message)</button>
    <button onclick="step12();">Step 2 (E intercepts message 0)</button>
    <button onclick="step13();">Step 3 (E transmits message 0)</button>
</div>

<script>
    class Listener extends Agent {
	process (msg, step) {
	    var facts = msg.content.getFacts();
	    for (var i = 0; i < facts.length; i++) {
		this.learns(facts[i], step);
	    }
	}
    }

    var s1 = new NetStack('message_list');
    var a1 = new Agent('A', ['secret', 'K', '{secret}K'], '');
    var b1 = new Listener('B', ['K'], '');
    var e1 = new Listener('E', [], '');

    s1.registerAgents([a1, b1, e1]);
    s1.registerAttacker(e1);

    a1.createDiv('agents_box', 'agent');      
    b1.createDiv('agents_box', 'agent');
    e1.createDiv('agents_box', 'adversary');

setCurrentStep(0);

function step10 () {
    clearBox(0, 'box1');
    b1.forgets(0);
    b1.updateContent();
    s1.clearStack(0);
    s1.refreshStack();
    setCurrentStep(0);
}

function step11 () {
    step10();
    clearBox(1, 'box1');
    s1.clearStack(1);
    s1.addMessage(create_message('A -> B: {secret}K'), 1, 'w', 'box1');
    s1.refreshStack();
    setCurrentStep(1);	
    
}
function step12 () {
    step11();
    clearBox(2, 'box1');
    s1.interceptMessage(0, 2, 'box1');
    s1.refreshStack();
    e1.updateContent();
    setCurrentStep(2);	
    
}
function step13 () {
    step12();
    s1.transmitMessage(0, 3, 'box1');
    s1.refreshStack();
    b1.learns('secret', 3);
    b1.updateContent();
    setCurrentStep(3);	
    
}

</script>

Let us first observe that even though Alice's initial knowledge is [secret, K], we automatically added "{secret}K". 
Indeed, any agent knowing a message "m" and a key "K" can do the encryption and will then know the resulting 
cipher-text. In general, we try to only add to the knowledge of an agent information that is relevant. For instance, 
we could also add "{{secret}K}K" or "{K}K" to the knowledge of Alice, however this is not required by the protocol.

Similarly, as soon as Bob receives "{secret}K", we can add "secret" to his knowledge, since he also knows the key "K". 
On the other hand, since Eve does not know "K", even though she can intercept "{secret}K", she cannot know "secret" 
since she cannot do the decryption.


{{< protocolgame_svg >}}
