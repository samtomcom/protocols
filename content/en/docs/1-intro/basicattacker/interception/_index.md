---
title: "Interception"
date: 2020-02-17
weight: 10
---
{{< pg_start >}}

    
### Interception

 Let us consider an active adversary Eve (usually referred as E), located on the right hand side of the network bar. The first possible attack is for Eve to intercept any message, e.g., in the case of the previous protocol, to intercept the message "secret" from Alice to Bob.

 We therefore introduce a new status "/i", which indicates that the message has been intercepted (and the message should be underlined). 

<div class='game'>
    <div class='agents' id='agents_box1'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list1'></ul>
    </div>
</div>
 
  <div class='steps'>
    <button onclick="step10();">Step 0</button>
    <button onclick="step11();">Step 1 (A sends message)</button>
    <button onclick="step12();">Step 2 (E intercepts message)</button>
    <button onclick="step13();">Step 3 (message transmitted)</button>
  </div>


<script>
class Listener extends Agent {
    process (msg, step) {
	var facts = msg.content.getFacts();
	for (var i = 0; i < facts.length; i++) {
	    this.learns(facts[i], step);
	}
    }
}
var s1 = new NetStack('message_list1');
var a1 = new Agent('A', ['secret'], s1);
var b1 = new Listener('B', [], s1);
var e1 = new Listener('E', [], s1);
s1.registerAgents([a1, b1, e1]);
s1.registerAttacker(e1);

a1.learns('secret', 0);
a1.createDiv('agents_box1', 'agent');      
b1.createDiv('agents_box1', 'agent');
e1.createDiv('agents_box1', 'adversary');

setCurrentStep(0);

function step10 () {
    clearBox(0, 'box1');
    s1.clearStack(0);
    s1.refreshStack();
    b1.forgets(0);
    b1.updateContent();
    e1.forgets(0);
    e1.updateContent();
    setCurrentStep(0);
}

function step11 () {
    step10();
    clearBox(1, 'box1');
    s1.addMessage(create_message('A -> B: secret'), 1, 'w', 'box1');
    s1.refreshStack();
    setCurrentStep(1);	
    
}
function step12 () {
    step11();
    clearBox(2, 'box1');
    s1.interceptMessage(0, 2, 'box1')
    e1.updateContent();
    s1.refreshStack();
    setCurrentStep(2);   	      
}


function step13 () {
    step12();
    s1.transmitMessage(0, 3, 'box1');
    b1.updateContent();
    s1.refreshStack();
    setCurrentStep(3);	
    
}
</script>


<br><br>
Step 2 corresponds to the interception of the message on the network, after which Eve  knows the message "secret". 
  


  {{< protocolgame_svg >}}
