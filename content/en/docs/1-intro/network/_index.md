---
title: "Network"
date: 2020-02-17
weight: 20
---
{{< pg_start >}}

The main goal of this program is to take the role of an attacker, targeting communications exchanged between two parties (usually A and B). The game is essentially interactive: the attacker executes a command  and the agents in the protocol react accordingly. In the next pages, we go through the different concepts used in this game. 

Traditionally speaking, we consider the case where two agents, Alice and Bob, want to exchange some messages over a network (we usually simply use in diagrams A for Alice, and B for Bob). A protocol defines how Alice and Bob exchange messages, and takes the form of a sequence of steps, each step indicating the sender, the receiver, and the content of the message exchanged. 

For instance, imagine a situation where Alice wants to send the private message "secret" to Bob. A very simple protocol could be: 
<ol>
  <li> A -> B : secret
</ol>

Although this description is relatively straightforward, let us try to analyse it, using a graphical representation of the different elements (you can click on the different steps to see the animation).

We introduce the network as a grey bar located below the agents. To visualise network communications, arrows between the sender and the network are associated with a message number (e.g., [0] for the first message sent on the network), and the full message can be found in the Network Stack. Each message in the stack is associated with a message number, and a status which starts with the "/" symbol. We consider two statuses at this stage:
    <ul>
      <li> "/w" indicates that the message is waiting in the stack (and the message should be displayed in italic). 
      <li> "/t" indicates that the message has been transmitted to its intended recipient (and the message should be displayed in green). 
    </ul>

<div class='game'>
    <div class='agents' id='agents_box1'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list'>
        </ul>
    </div>
  <div class='steps'>
    <button onclick="step0();">Step 0</button>
    <button onclick="step1();">Step 1 (A sends message)</button>
    <button onclick="step2();">Step 2 (message transmitted)</button>
  </div>
</div>



<script>

class Listener extends Agent {
    process (msg, step) {
	var facts = msg.content.getFacts();
	for (var i = 0; i < facts.length; i++) {
	    this.learns(facts[i], step);
	}
    }
}

var s = new NetStack('message_list');
var a = new Agent('A', ['secret'], s);
var b = new Listener('B', [], s);
s.registerAgents([a, b]);

a.createDiv('agents_box1', 'agent');      
b.createDiv('agents_box1', 'agent');
setCurrentStep(0);

function step0 () {
    clearAll(0);
    b.forgets(0);
    b.updateContent();
    s.clearStack(0);
    s.refreshStack();
    setCurrentStep(0);
}

function step1 () {
    step0();
    s.addMessage(create_message('A -> B: secret'), 1, 'w');
    s.refreshStack();
    setCurrentStep(1);	

}

function step2 () {
    step1();
    s.transmitMessage(0, 2);
    b.updateContent();
    s.refreshStack();
    setCurrentStep(2);	

}

  </script>

  {{< protocolgame_svg >}}
