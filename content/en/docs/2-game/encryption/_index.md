 ---
title: "Encryption"
date: 2020-02-16
weight: 20
---
{{< pg_start >}}

The previous protocol is not secure at it clearly sends the message in plain-text, and so it's enough to intercept it. The protocol below uses some encryption: B will send a key to A, and A will use that key to encrypt the secret.

<b>Knowledge:</b> A:[s, hello], B:[K], E:[fake]<br>
<b>Steps:</b>
  <ol>
    <li> A -> B: hello
    <li> B -> A: #K
    <li> A -> B: {#s}K
  </ol>


We therefore introduce commands related to encryption: 

 * <span class='cmd'>encrypt(fact, key)</span>: if E knows both <span class='cmd'>fact</span> and <span class='cmd'>key</span>, then E will know  <span class='cmd'>{fact}key</span>; 
 * <span class='cmd'>decrypt({fact}key, key)</span>: if E knows both <span class='cmd'>{fact}key</span> and <span class='cmd'>key</span>, then E will know  <span class='cmd'>fact</span>; 

If you try the same attack as the previous step, it won't be enough, since you can only intercept the encrypted message. 
	  
{{< protocolgame >}}

<script>

 class Sender extends Agent {
	      
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.session = 0;
		  this.state = 0;
	      }
	      
	      // Sender has the following states
	      // init: initial state, ready to send message 1
	      // (in case of multiple sessions, message 3 from previous session
	      // has been sent)
	      // waiting_2: message 1 sent, waiting for message 2. 
	      
	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.init(step); break;
		  case 1:
		      this.sendEncryptedSecret(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      getSessionSecret (step) {
		  var secret = 's_' + this.session;
		  this.learns(secret, step)
		  return secret;
	      }
	      
	      sendEncryptedSecret(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  var k = facts[0];
		  this.learns(k, step);
		  
		  var new_msg = {source:this.id, destination:'B',
				 content:this.encrypt(this.getSessionSecret(step), k, step)};
		  this.sendMessage(new_msg, step);
		  this.state = 0;
		  
	      }
	      
	      init (step) {
		  var msg = {source:this.id, destination:'B', content:'hello'}; 
		  this.sendMessage(msg, step);
		  this.session++;
		  this.state = 1;
	      }
	  }

	  class Receiver extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.session = 0;
		  this.state = 0;
		  this.key = null;
	      }

	      process(msg, step) {
		  switch (this.state) {
		  case 0:
		      this.sendKey(msg, step); break;
		  case 1:
		      this.decryptMessage(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		      
		  }


	      }

	      getFreshKey(step) {
		  var k = 'K_' + this.session;
		  this.learns(k, step)
		  this.key = k;
		  return k;
	      }

	      
	      sendKey(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  var hello = facts[0];
		  if (hello != 'hello') { 
		      console.log('Expecting hello, received: ' + hello);
		      return;
		  }
		  this.learns(hello, step);
		  var new_msg = {source:this.id, destination:'A',
				 content:this.getFreshKey(step)};
		  this.sendMessage(new_msg, step);
		  this.state = 1;
	      }

	      decryptMessage(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  this.learns(facts[0]);
		  this.decrypt(facts[0], this.key, step);
		  this.step = 0; 
	      }

	  }

	  class Attacker extends Agent {
	      process (msg) {
		  this.learns(msg.content.getFacts()[0]);
	      }
	  }
	  
	  var s1 = new NetStack('message_list1');	  
	  var a1 = new Sender('A', [], s1);
	  var b1 = new Receiver('B', [], s1);
	  var e1 = new Attacker('E', [], s1);
	  s1.registerAgents([a1, b1, e1]);
	  s1.registerAttacker(e1);
	  var c1 = new Command(s1, e1, [a1, b1, e1]);
	  a1.learns('s', 0);
	  a1.learns('hello', 0);
	  b1.learns('K', 0);
	  e1.learns('fake', 0);
	  a1.createDiv('agents_box1', 'agent');      
	  b1.createDiv('agents_box1', 'agent');
	  e1.createDiv('agents_box1', 'agent');

	  setCurrentStep(0);
	  

  
</script>

## Attacks

1. Can you find a sequence of commands such that E knows s_1? 
* {{< tip Tip tip1 >}}
You probably need to intercept multiple messages, and do some decryption. You also need to let the protocol run by transmitting the messages. 
{{< /tip >}}

* {{< tip Solution sol1 >}}
<span class='cmd'>
new_session();
transmit(0);intercept(1);transmit(1);intercept(2);
decrypt({s_1}K_0, K_0);
</span>
{{< /tip >}}


2. Can you find a sequence of commands such that B knows fake? 

* {{< tip Tip tip2 >}}
B is expecting something encrypted with the key it sent. 
{{< /tip >}}

* {{< tip Solution sol2 >}}
<span class='cmd'>new_session(); transmit(0);intercept(1);
encrypt(fake, K_0);
inject(A->B:{fake}K_0);
transmit(2);</span>
{{< /tip >}}

{{< protocolgame_svg >}}
